# Plugginaway

A sample app for showing off some plug stuff for a meetup.

## Resources

* https://hexdocs.pm/plug/readme.html
* https://github.com/IdahoEv/cowboy-elixir-example
* https://github.com/elixir-ecto/postgrex
* https://github.com/wg/wrk
* http://elixir-lang.org/getting-started/mix-otp/agent.html
* http://www.erlang.org/doc/man/ets.html

## Continued Reading
* https://github.com/riverrun/comeonin
* https://hexdocs.pm/comeonin/Comeonin.html

## Installation & Running the load test

* clone the repo
* `mix deps.get`
* `mix do clean, compile.deps, compile`
* `mix -S iex`
* (in another console) `script/load-test`

# $ wrk --latency -c30 -t8 -d15 'http://localhost:4000/?secret_key=1234567890abcdef1234567890abcdef'
# Running 15s test @ http://localhost:4000/?secret_key=1234567890abcdef1234567890abcdef
#   8 threads and 30 connections
#   Thread Stats   Avg      Stdev     Max   +/- Stdev
#     Latency    11.30ms   53.44ms 577.49ms   96.63%
#     Req/Sec     1.44k   317.10     1.98k    90.33%
#   Latency Distribution
#      50%    1.68ms
#      75%    2.81ms
#      90%    7.21ms
#      99%  350.49ms
#   167505 requests in 15.02s, 31.66MB read
# Requests/sec:  11151.25
# Transfer/sec:      2.11MB

defmodule Plugginaway.Time do
  @doc """
  Unix time AKA seconds since epoch (1970-01-01).
  """
  @spec unix_time :: integer
  def unix_time do
    :os.system_time(:seconds)
  end
end

defmodule Plugginaway.Cache do
  use GenServer

  alias Plugginaway.Time

  @ttl_seconds 30
  @ets_table :users_cache

  ## GenServer Client API

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, @ets_table, opts)
  end

  def get(key, fallback \\ fn -> nil end) do
    now = Time.unix_time
    case :ets.lookup(@ets_table, key) do
      [{^key, {value, ttl}}] when (now - ttl) < @ttl_seconds ->
        value
      _ ->
        value = fallback.()
        set(key, value)
        value
    end
  end

  def set(key, value, time_added \\ Time.unix_time) do
    :ets.insert(@ets_table, {key, {value, time_added}})
  end

  ## GenServer Server Callbacks
  def init(table) do
    ets = :ets.new(table, [:set, :public, :named_table, {:read_concurrency, true}])
    {:ok, %{cache: ets}}
  end
end

defmodule Plugginaway.Plug.SecretKeyExtractor do
  import Plug.Conn

  @secret_key_key "secret_key"

  def init(options), do: options

  def call(conn, _options) do
    conn = conn |> fetch_query_params
    secret_key = parse_key(conn.query_params[@secret_key_key])
    # we could halt processing here, but choose instead to
    # handle it at a later point.
    conn |> assign(:secret_key, secret_key)
  end

  defp parse_key(nil), do: nil
  defp parse_key(""), do: nil
  defp parse_key(key), do: key
end

defmodule Plugginaway do
  use Plug.Builder
  plug Plug.Logger
  plug Plugginaway.Plug.SecretKeyExtractor

  @database                    "indy-ex-db"
  @user_prepared_statment_name "user_lookup"
  @user_query                  "SELECT * FROM USERS WHERE secret_key = $1 AND enabled = TRUE"

  @response_content_type "text/plain"
  @response_success      "Hello world"
  @response_miss         "Hello REDACTED"

  # startup postgrex, cowboy, and cache
  def start(_type, _args) do
    {:ok, postgrex} = Postgrex.start_link(
      database: @database,
      backoff_type: :stop)
    {:ok, lookup} = postgrex |> Postgrex.prepare(@user_prepared_statment_name, @user_query)
    {:ok, _cache} = Plugginaway.Cache.start_link
    {:ok, _cowboy} = Plug.Adapters.Cowboy.http(__MODULE__, [postgrex: postgrex, lookup: lookup])
  end

  def call(conn, opts) do
    conn =
      super(conn, opts)
      |> put_resp_content_type(@response_content_type)

    case find_by_secret_key(opts[:postgrex], opts[:lookup], conn.assigns[:secret_key]) do
      {:ok, nil} -> conn |> send_resp(404, @response_miss)
      {:ok, id} -> conn |> send_resp(200, "#{@response_success} #{id}")
      {:error, error} -> conn |> send_resp(500, error)
    end
  end

  defp find_by_secret_key(conn, query, secret_key) do
    Plugginaway.Cache.get(secret_key, fn ->
      case Postgrex.execute(
            conn,
            query,
            [secret_key]) do
        {:ok, %Postgrex.Result{rows: nil}} -> {:ok, nil}
        {:ok, %Postgrex.Result{rows: []}} -> {:ok, nil}
        {:ok, %Postgrex.Result{rows: [[id, _detail, _secret_key, _secret_hash, true]]}} -> {:ok, id}
        {:error, %Postgrex.Error{} = error} -> {:error, error}
      end
    end)
  end
end
